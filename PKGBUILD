# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

# Arch credits:
# Based on the file created for Arch Linux by:
# Massimiliano Torromeo <massimiliano.torromeo@gmail.com>
# Bob Fanger <bfanger(at)gmail>
# Filip <fila pruda com>, Det <nimetonmaili(at)gmail>

_linuxprefix=linux-zen
_extramodules=extramodules-6.2-zen
pkgname=$_linuxprefix-r8168
_pkgname=r8168
_pkgver=8.050.03
pkgver=8.050.03_6.2.10.zen1_1
pkgrel=1
pkgdesc="A kernel module for Realtek 8168 network cards"
url="http://www.realtek.com.tw"
license=("GPL")
arch=('x86_64')
depends=('glibc' "$_linuxprefix")
makedepends=("$_linuxprefix-headers")
provides=("$_pkgname=$_pkgver")
groups=("$_linuxprefix-extramodules")
source=("https://github.com/mtorromeo/r8168/archive/$_pkgver/$_pkgname-$_pkgver.tar.gz"
        'linux518.patch' 'linux519.patch' 'linux61.patch')
sha256sums=('76f9e7c26a8ade7b01dd34060f5b17d74387f15e9b6baa6dbba8c43634a31ce6'
            'd8d542770e504775600f686d03412a37cc32489872be7aeb388b5b08d9806096'
            'f19c663f278096a93b2fc80222e208a54ab8677f6d7eeb9c15150c7c55ec2eff'
            'b43a2ec8270124afe6fa23fafc1be156779e9a0d47db22e1583b60891bd286d5')

install=$_pkgname.install

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2 | cut -f1-2 -d'-')
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

prepare() {
    cd "$_pkgname-$_pkgver"
    patch -Np1 -i ${srcdir}/linux518.patch
    patch -Np1 -i ${srcdir}/linux519.patch
    patch -Np1 -i ${srcdir}/linux61.patch
}

build() {
    _kernver="$(cat /usr/lib/modules/$_extramodules/version || true)"

    cd "$_pkgname-$_pkgver"

    # avoid using the Makefile directly -- it doesn't understand
    # any kernel but the current.

    make -C /usr/lib/modules/$_kernver/build \
      M="$srcdir/$_pkgname-$_pkgver/src" \
      EXTRA_CFLAGS="-DCONFIG_R8168_NAPI -DCONFIG_R8168_VLAN -DCONFIG_ASPM -DENABLE_S5WOL -DENABLE_EEE" \
      modules
}

package() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    depends=("${_linuxprefix}=${_ver}")

    cd "$srcdir/$_pkgname-$_pkgver/src"
    install -D -m644 $_pkgname.ko "$pkgdir/usr/lib/modules/$_extramodules/$_pkgname.ko"

    # set the kernel we've built for inside the install script
    sed -i -e "s/EXTRAMODULES=.*/EXTRAMODULES=${_extramodules}/g" "${startdir}/${_pkgname}.install"

    find "$pkgdir" -name '*.ko' -exec gzip -9 {} \;
}
